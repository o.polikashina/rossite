$(function() {
    $(".btn-open-form").on("click", function(){
        setTimeout(function() { $('#formModal').modal('toggle'); }, 5000); 
    });
    $("#formModal").on("shown.bs.modal", function () {
        $("#userName").trigger("focus")
    });
    $(".bnt-send-data").on("click", function() {
        let err = [],
            userName = $("#userName"),
            userPhone = $("#userPhone"),
            userEmail = $("#userEmail"),
            userNameVal = userName.val().trim(),
            userPhoneVal = userPhone.val().trim(),
            userEmailVal = userEmail.val().trim();
        if (userNameVal.length === 0){
            err.push("имя");
        }
        if (userPhoneVal.length === 0){
            err.push("телефон");
        }
        if (userEmailVal.length === 0){
            err.push("email");
        }
        else if (!validateEmail(userEmailVal)){
            err.push("корректный email");
        }
        if (err.length > 0){
            $(".data-error").html("Введите " + err.join(", ")).show();
            return;
        }
        console.log("ajax")
        $.ajax({
            url: "https://httpbin.org/get",
        })
        .always(function( data ) {
            toggleElements();
            $(".saved-data-name").text(userNameVal);
            $(".saved-data-phone").text(userPhoneVal);
            $(".saved-data-email").text(userEmailVal);
            $('#formModal').modal('hide');
        });
    });
    $(".btn-clear-data").on("click", function() {
        toggleElements();
        clearForm();
    });
    $('#userPhone').on('keydown', function(evt) {
        var key = evt.charCode || evt.keyCode || 0;
        return (key == 8 ||
                key == 9 ||
                key == 46 ||
                key == 110 ||
                key == 190 ||
                (key >= 35 && key <= 40) ||
                (key >= 48 && key <= 57) ||
                (key >= 96 && key <= 105));
    });
    function validateEmail(email) {
        var re = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;
        return re.test(String(email).toLowerCase());
    }
    function toggleElements(){
        $(".btn-open-form").toggle();
        $(".saved-data").toggle();
    }
    function clearForm(){
        $(".data-error").hide().empty();
        $(".saved-data-name").empty();
        $(".saved-data-phone").empty();
        $(".saved-data-email").empty();
        $(".form-user-data")[0].reset();
    }
})


